const sumar = require("./sumar");
const restar = require("./restar");
const dividir = require("./dividir");
const multiplicar = require("./multiplicar");

function factory(op, data) {
    if (op === "+") {
      return function() {
        return sumar(data.a , data.b);
      };
    }
  
    if (op === "-") {
      return function() {
        return restar(data.a , data.b);
      };
    }
  
    if (op === "*") {
      return function() {
        return multiplicar(data.a , data.b);
      };
    }
  
    if (op === "/") {
      return function() {
        return dividir(data.a , data.b);
      };
    }
  }
  
  const sum = factory("+", { a: 2, b: 2 });
  const res = factory("-", { a: 2, b: 2 });
  const mul = factory("*", { a: 2, b: 2 });
  const div = factory("/", { a: 2, b: 2 });
  
  console.log(sum());
  console.log(res());
  console.log(mul());
  console.log(div());
  